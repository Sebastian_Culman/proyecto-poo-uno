@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Datos del Cliente</div>

                <div class='col text-right'>

                </div>
                  <a href='{{ route('crear.cliente') }}' class='btn btn-sm btn-primary'>Nuevo Registro</a>
                <div class="card-body">
                  <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">Nombre</th>
                        <th scope="col">Apellidos</th>
                        <th scope="col">Cédula</th>
                        <th scope="col">Dirección</th>
                        <th scope="col">Teléfono</th>
                        <th scope="col">Fecha de Nacimiento</th>
                        <th scope="col">Email</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach($cliente as $item)
    
                      <tr>
                        <th scope="row">{{$item->nombre}}</th>
                        <td>{{$item->apellidos}}</td>
                        <td>{{$item->cédula}}</td>
                        <td>{{$item->dirección}}</td>
                        <td>{{$item->teléfono}}</td>
                        <td>{{$item->fecha_nacimiento}}</td>
                        <td>{{$item->email}}</td>
                      </tr>
                        @endforeach
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
