<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cliente extends Model
{
    protected $fillable = [
        'nombre',
        'apellidos',
        'cédula',
        'dirección',
        'teléfono',
        'fecha_nacimiento',
        'email',
    ];
}
