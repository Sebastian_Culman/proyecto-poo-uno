<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cliente;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function Cliente(Request $request)
    {
        $cliente = cliente::all();
        //dd($cliente);
        return view('cliente.cliente')->with('cliente',$cliente);
    }

    public function CrearRegistro(Request $request){
        $cliente = cliente::all();
        return view('cliente.crear')->with('cliente',$cliente);
    }

    public function GuardarRegistro(Request $request){

        $this->validate($request, [
            'nombre'=>'required',
            'apellidos'=>'required',
            'cédula'=>'required',
            'dirección'=>'required',
            'teléfono'=>'required',
            'fecha_nacimiento'=>'required',
            'email'=>'required',
        ]);

        $cliente = new Cliente;
        $cliente -> nombre = $request->nombre;
        $cliente -> apellidos = $request->apellidos;
        $cliente -> cédula = $request->cédula;
        $cliente -> dirección = $request->dirección;
        $cliente -> teléfono = $request->teléfono;
        $cliente -> fecha_nacimiento = $request->fecha_nacimiento;
        $cliente -> email = $request->email;
        $cliente -> save();
        return redirect()->route('data.cliente');
    }
     
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
